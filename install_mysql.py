#!/usr/bin/python3

import subprocess

subprocess.run(['sudo', 'apt-get', 'update'])
subprocess.run(['sudo', 'apt-get', '-y', 'install', 'mysql-server'])

subprocess.run(['sudo', 'service', 'mysql', 'start'])

subprocess.run(['sudo', 'mysql', '-e', "CREATE USER 'adminbd'@'localhost' IDENTIFIED BY 'qazwsx';"])
subprocess.run(['sudo', 'mysql', '-e', "GRANT ALL PRIVILEGES ON *.* TO 'adminbd'@'localhost';"])
subprocess.run(['sudo', 'mysql', '-e', 'FLUSH PRIVILEGES'])

print('MySQL was installed and configure for adminbd with password qazwsx.')
