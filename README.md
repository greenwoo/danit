## Table IP

SUBNET 10.233.0.0/8

CLIENTS:
10.233.50.0-254/8

DNS:
10.233.10.5/8  //Main DNS (Kyiv)
10.233.10.7/8  //Slave DNS (Kyiv)
10.233.10.9/8  //Reserv slave DNS (Kyiv Oblast)

VPN IN:
10.233.0.1/8

SERVERS:
10.233.0.2/8  //FIREWALL
10.233.15.1/8 //ANSIBLE - BASTION
10.233.15.2/8 //GITLAB
10.233.15.3/8 // Grafana - Prometheus
10.233.15.4/8 // Mattermost
10.233.15.4/8 // Jira
10.233.15.5/8 // Rsyslog
10.233.15.6/8 //MAIL





## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

