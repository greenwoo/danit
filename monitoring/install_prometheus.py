#!/usr/bin/python3

import subprocess


def update_system():
    print("Update the system")
    subprocess.run(['sudo', 'apt-get', 'update'])
    subprocess.run(['sudo', 'apt-get', 'full-upgrade', '-y'])
    subprocess.run(['sudo', 'apt-get', 'autoremove', '-y'])

def install_prometheus():
    print("Install Prometheus")
    download_link = "https://github.com/prometheus/prometheus/releases/download/v2.45.3/prometheus-2.45.3.linux-amd64.tar.gz"
    subprocess.run(download_link, shell=True)
    extract_command = "tar xvfz prometheus-*.tar.gz"
    subprocess.run(extract_command, shell=True)
    remove_command = "rm prometheus-*.tar.gz"
    subprocess.run(remove_command, shell=True)
    subprocess.run("sudo mkdir /etc/prometheus /var/lib/prometheus", shell=True)
    subprocess.run("cd prometheus-2.45.3.linux-amd64 && sudo mv prometheus promtool /usr/local/bin/", shell=True)
    subprocess.run("sudo mv prometheus.yml /etc/prometheus/prometheus.yml", shell=True)
    subprocess.run("sudo mv consoles/ console_libraries/ /etc/prometheus/", shell=True)
    version_command = "prometheus --version"
    subprocess.run(version_command, shell=True)

def start_prometheus():
    print("Start Prometheus")
    subprocess.run("sudo useradd -rs /bin/false prometheus", shell=True)
    subprocess.run("sudo chown -R prometheus: /etc/prometheus /var/lib/prometheus", shell=True)
    systemd_file_content = """
    [Unit]
    Description=Prometheus
    Wants=network-online.target
    After=network-online.target

    [Service]
    User=prometheus
    Group=prometheus
    Type=simple
    ExecStart=/usr/local/bin/prometheus \
        --config.file /etc/prometheus/prometheus.yml \
        --storage.tsdb.path /var/lib/prometheus/ \
        --web.console.templates=/etc/prometheus/consoles \
        --web.console.libraries=/etc/prometheus/console_libraries \
        --web.listen-address=0.0.0.0:9090 \
        --web.enable-lifecycle \
        --log.level=info

    [Install]
    WantedBy=default.target
    """
    with open("/etc/systemd/system/prometheus.service", "w") as file:
        file.write(systemd_file_content)
    subprocess.run("sudo systemctl daemon-reload", shell=True)
    subprocess.run("sudo systemctl enable prometheus", shell=True)
    subprocess.run("sudo systemctl start prometheus", shell=True)

def main():
    update_system()
    install_prometheus()
    start_prometheus()

if __name__ == '__main__':
    main()
